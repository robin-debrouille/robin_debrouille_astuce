<?php

/**
 * @file
 * robin_debrouille_astuce.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function robin_debrouille_astuce_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels" && $api == "layouts") {
    return array("version" => "1");
  }
  if ($module == "panels" && $api == "pipelines") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function robin_debrouille_astuce_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function robin_debrouille_astuce_node_info() {
  $items = array(
    'astuce' => array(
      'name' => t('Astuces'),
      'base' => 'node_content',
      'description' => t('tous les astuces cuisine, bricolage, jardinage,informatique...'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'etape' => array(
      'name' => t('Étape'),
      'base' => 'node_content',
      'description' => t('toutes les étapes pour bon tuto'),
      'has_title' => '1',
      'title_label' => t('titre de l\'étape'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
