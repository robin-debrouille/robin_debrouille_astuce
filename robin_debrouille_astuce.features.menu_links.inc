<?php

/**
 * @file
 * robin_debrouille_astuce.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function robin_debrouille_astuce_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_actus:blog.
  $menu_links['main-menu_actus:blog'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blog',
    'router_path' => 'blog',
    'link_title' => 'Actus',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_actus:blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_astuces:astuces.
  $menu_links['main-menu_astuces:astuces'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'astuces',
    'router_path' => 'astuces',
    'link_title' => 'Astuces',
    'options' => array(
      'attributes' => array(
        'title' => 'Voir les astuces',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_astuces:astuces',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_publier:node/add/astuce.
  $menu_links['main-menu_publier:node/add/astuce'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add/astuce',
    'router_path' => 'node/add/astuce',
    'link_title' => 'Publier',
    'options' => array(
      'attributes' => array(
        'title' => 'Publiez votre astuce',
      ),
      'alter' => TRUE,
      'identifier' => 'main-menu_publier:node/add/astuce',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-segondaire_accueil:<front>.
  $menu_links['menu-menu-segondaire_accueil:<front>'] = array(
    'menu_name' => 'menu-menu-segondaire',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Accueil',
    'options' => array(
      'attributes' => array(
        'title' => 'Revenir vers l\'accueil',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-menu-segondaire_accueil:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-segondaire_astuces:astuces.
  $menu_links['menu-menu-segondaire_astuces:astuces'] = array(
    'menu_name' => 'menu-menu-segondaire',
    'link_path' => 'astuces',
    'router_path' => 'astuces',
    'link_title' => 'Astuces',
    'options' => array(
      'attributes' => array(
        'title' => 'Voir les astuces',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-menu-segondaire_astuces:astuces',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-segondaire_blog:blog.
  $menu_links['menu-menu-segondaire_blog:blog'] = array(
    'menu_name' => 'menu-menu-segondaire',
    'link_path' => 'blog',
    'router_path' => 'blog',
    'link_title' => 'Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-menu-segondaire_blog:blog',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: menu-menu-segondaire_publier:node/add/astuce.
  $menu_links['menu-menu-segondaire_publier:node/add/astuce'] = array(
    'menu_name' => 'menu-menu-segondaire',
    'link_path' => 'node/add/astuce',
    'router_path' => 'node/add/astuce',
    'link_title' => 'Publier',
    'options' => array(
      'attributes' => array(
        'title' => 'Publiez votre astuce',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-menu-segondaire_publier:node/add/astuce',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: user-menu_ajouter-une-astuce:node/add/astuce.
  $menu_links['user-menu_ajouter-une-astuce:node/add/astuce'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'node/add/astuce',
    'router_path' => 'node/add/astuce',
    'link_title' => 'ajouter une astuce',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
      'identifier' => 'user-menu_ajouter-une-astuce:node/add/astuce',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: user-menu_log-out:user/logout.
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_log-out:user/logout',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 0,
  );
  // Exported menu link: user-menu_user-account:user.
  $menu_links['user-menu_user-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_user-account:user',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -10,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Accueil');
  t('Actus');
  t('Astuces');
  t('Blog');
  t('Log out');
  t('Publier');
  t('User account');
  t('ajouter une astuce');

  return $menu_links;
}
