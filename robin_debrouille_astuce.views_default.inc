<?php
/**
 * @file
 * robin_debrouille.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function robin_debrouille_astuce_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'astuces';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Astuces';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Mes astuces';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'plus';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Éléments par page';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Tout -';
  $handler->display->display_options['pager']['options']['expose']['offset_label'] = 'Décalage';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« premier';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ précédent';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'suivant ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'dernier »';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Champ: Contenu : image */
  $handler->display->display_options['fields']['field_iilustration_astuce']['id'] = 'field_iilustration_astuce';
  $handler->display->display_options['fields']['field_iilustration_astuce']['table'] = 'field_data_field_iilustration_astuce';
  $handler->display->display_options['fields']['field_iilustration_astuce']['field'] = 'field_iilustration_astuce';
  $handler->display->display_options['fields']['field_iilustration_astuce']['label'] = '';
  $handler->display->display_options['fields']['field_iilustration_astuce']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_iilustration_astuce']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_iilustration_astuce']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => 'content',
  );
  /* Champ: Contenu : Titre */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '30';
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Champ: Champ : Catégorie */
  $handler->display->display_options['fields']['field_cat_gorie']['id'] = 'field_cat_gorie';
  $handler->display->display_options['fields']['field_cat_gorie']['table'] = 'field_data_field_cat_gorie';
  $handler->display->display_options['fields']['field_cat_gorie']['field'] = 'field_cat_gorie';
  $handler->display->display_options['fields']['field_cat_gorie']['label'] = 'Catégories';
  $handler->display->display_options['fields']['field_cat_gorie']['delta_offset'] = '0';
  /* Champ: Contenu : note */
  $handler->display->display_options['fields']['field_note']['id'] = 'field_note';
  $handler->display->display_options['fields']['field_note']['table'] = 'field_data_field_note';
  $handler->display->display_options['fields']['field_note']['field'] = 'field_note';
  $handler->display->display_options['fields']['field_note']['click_sort_column'] = 'rating';
  $handler->display->display_options['fields']['field_note']['settings'] = array(
    'widget' => array(
      'fivestar_widget' => 'sites/all/modules/fivestar/widgets/oxygen/oxygen.css',
    ),
    'expose' => 0,
    'style' => 'average',
    'text' => 'none',
  );
  /* Champ: Global : Texte personnalisé */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<img src="../sites/all/themes/robinDebrouille/img/chevron-en-savoir-plus.png"/>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Champ: Contenu : Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '150';
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Critère de tri: Contenu : Date de publication */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Date de publication';
  /* Critère de tri: Contenu : note (field_note:rating) */
  $handler->display->display_options['sorts']['field_note_rating']['id'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['table'] = 'field_data_field_note';
  $handler->display->display_options['sorts']['field_note_rating']['field'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_note_rating']['expose']['label'] = 'note';
  /* Critère de tri: Contenu : Difficulté (field_difficulte) */
  $handler->display->display_options['sorts']['field_difficulte_tid']['id'] = 'field_difficulte_tid';
  $handler->display->display_options['sorts']['field_difficulte_tid']['table'] = 'field_data_field_difficulte';
  $handler->display->display_options['sorts']['field_difficulte_tid']['field'] = 'field_difficulte_tid';
  $handler->display->display_options['sorts']['field_difficulte_tid']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_difficulte_tid']['expose']['label'] = 'Difficulté ';
  /* Critère de filtrage: Contenu : Publié */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critère de filtrage: Contenu : Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'astuce' => 'astuce',
  );
  /* Critère de filtrage: Contenu : Body (body) */
  $handler->display->display_options['filters']['body_value']['id'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['table'] = 'field_data_body';
  $handler->display->display_options['filters']['body_value']['field'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['body_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['body_value']['expose']['operator_id'] = 'body_value_op';
  $handler->display->display_options['filters']['body_value']['expose']['label'] = 'Contenu';
  $handler->display->display_options['filters']['body_value']['expose']['operator'] = 'body_value_op';
  $handler->display->display_options['filters']['body_value']['expose']['identifier'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['body_value']['group_info']['label'] = 'Body (body)';
  $handler->display->display_options['filters']['body_value']['group_info']['identifier'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['body_value']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Les astuces';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Champ: Contenu : Titre */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['max_length'] = '30';
  $handler->display->display_options['fields']['title']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Champ: Contenu : image */
  $handler->display->display_options['fields']['field_iilustration_astuce']['id'] = 'field_iilustration_astuce';
  $handler->display->display_options['fields']['field_iilustration_astuce']['table'] = 'field_data_field_iilustration_astuce';
  $handler->display->display_options['fields']['field_iilustration_astuce']['field'] = 'field_iilustration_astuce';
  $handler->display->display_options['fields']['field_iilustration_astuce']['label'] = '';
  $handler->display->display_options['fields']['field_iilustration_astuce']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_iilustration_astuce']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_iilustration_astuce']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => 'content',
  );
  /* Champ: Champ : Catégorie */
  $handler->display->display_options['fields']['field_cat_gorie']['id'] = 'field_cat_gorie';
  $handler->display->display_options['fields']['field_cat_gorie']['table'] = 'field_data_field_cat_gorie';
  $handler->display->display_options['fields']['field_cat_gorie']['field'] = 'field_cat_gorie';
  $handler->display->display_options['fields']['field_cat_gorie']['label'] = 'Catégories';
  $handler->display->display_options['fields']['field_cat_gorie']['delta_offset'] = '0';
  /* Champ: Contenu : note */
  $handler->display->display_options['fields']['field_note']['id'] = 'field_note';
  $handler->display->display_options['fields']['field_note']['table'] = 'field_data_field_note';
  $handler->display->display_options['fields']['field_note']['field'] = 'field_note';
  $handler->display->display_options['fields']['field_note']['click_sort_column'] = 'rating';
  $handler->display->display_options['fields']['field_note']['settings'] = array(
    'widget' => array(
      'fivestar_widget' => 'sites/all/modules/fivestar/widgets/oxygen/oxygen.css',
    ),
    'expose' => 0,
    'style' => 'average',
    'text' => 'none',
  );
  /* Champ: Global : Texte personnalisé */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<img src="../sites/all/themes/robinDebrouille/img/chevron-en-savoir-plus.png"/>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Champ: Contenu : Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '150';
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['path'] = 'astuces';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Astuces';
  $handler->display->display_options['menu']['name'] = 'main-menu';

  /* Display: Bloc meilleur */
  $handler = $view->new_display('block', 'Bloc meilleur', 'block_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Les meilleurs';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'astuce-accueil';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Critère de tri: Contenu : note (field_note:rating) */
  $handler->display->display_options['sorts']['field_note_rating']['id'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['table'] = 'field_data_field_note';
  $handler->display->display_options['sorts']['field_note_rating']['field'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_note_rating']['expose']['label'] = 'note';
  /* Critère de tri: Contenu : Date de publication */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Date de publication';

  /* Display: Bloc dernier */
  $handler = $view->new_display('block', 'Bloc dernier', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Les derniers';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'astuce-accueil';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Critère de tri: Contenu : Date de publication */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Date de publication';
  /* Critère de tri: Contenu : note (field_note:rating) */
  $handler->display->display_options['sorts']['field_note_rating']['id'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['table'] = 'field_data_field_note';
  $handler->display->display_options['sorts']['field_note_rating']['field'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_note_rating']['expose']['label'] = 'note';

  /* Display: Bloc coupe de coeur */
  $handler = $view->new_display('block', 'Bloc coupe de coeur', 'block_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Les coups de coeur';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'astuce-accueil';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Critère de tri: Contenu : Date de publication */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['sorts']['created']['expose']['label'] = 'Date de publication';
  /* Critère de tri: Contenu : note (field_note:rating) */
  $handler->display->display_options['sorts']['field_note_rating']['id'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['table'] = 'field_data_field_note';
  $handler->display->display_options['sorts']['field_note_rating']['field'] = 'field_note_rating';
  $handler->display->display_options['sorts']['field_note_rating']['order'] = 'DESC';
  $handler->display->display_options['sorts']['field_note_rating']['expose']['label'] = 'note';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Critère de filtrage: Contenu : Publié */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Critère de filtrage: Contenu : Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'astuce' => 'astuce',
  );
  /* Critère de filtrage: Contenu : Body (body) */
  $handler->display->display_options['filters']['body_value']['id'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['table'] = 'field_data_body';
  $handler->display->display_options['filters']['body_value']['field'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['body_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['body_value']['expose']['operator_id'] = 'body_value_op';
  $handler->display->display_options['filters']['body_value']['expose']['label'] = 'Contenu';
  $handler->display->display_options['filters']['body_value']['expose']['operator'] = 'body_value_op';
  $handler->display->display_options['filters']['body_value']['expose']['identifier'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['body_value']['group_info']['label'] = 'Body (body)';
  $handler->display->display_options['filters']['body_value']['group_info']['identifier'] = 'body_value';
  $handler->display->display_options['filters']['body_value']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['body_value']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  /* Critère de filtrage: Champ : Catégorie (field_cat_gorie) */
  $handler->display->display_options['filters']['field_cat_gorie_tid']['id'] = 'field_cat_gorie_tid';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['table'] = 'field_data_field_cat_gorie';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['field'] = 'field_cat_gorie_tid';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['value'] = '';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_cat_gorie_tid']['expose']['operator_id'] = 'field_cat_gorie_tid_op';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['expose']['label'] = 'Catégorie';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['expose']['operator'] = 'field_cat_gorie_tid_op';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['expose']['identifier'] = 'field_cat_gorie_tid';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_cat_gorie_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_cat_gorie_tid']['vocabulary'] = 'categorie';
  /* Critère de filtrage: Contenu : Difficulté (field_difficulte) */
  $handler->display->display_options['filters']['field_difficulte_tid']['id'] = 'field_difficulte_tid';
  $handler->display->display_options['filters']['field_difficulte_tid']['table'] = 'field_data_field_difficulte';
  $handler->display->display_options['filters']['field_difficulte_tid']['field'] = 'field_difficulte_tid';
  $handler->display->display_options['filters']['field_difficulte_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_difficulte_tid']['expose']['operator_id'] = 'field_difficulte_tid_op';
  $handler->display->display_options['filters']['field_difficulte_tid']['expose']['label'] = 'Difficulté';
  $handler->display->display_options['filters']['field_difficulte_tid']['expose']['operator'] = 'field_difficulte_tid_op';
  $handler->display->display_options['filters']['field_difficulte_tid']['expose']['identifier'] = 'field_difficulte_tid';
  $handler->display->display_options['filters']['field_difficulte_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_difficulte_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_difficulte_tid']['vocabulary'] = 'difficulte';
  /* Critère de filtrage: Contenu : coup de coeur (field_coup_de_coeur) */
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['id'] = 'field_coup_de_coeur_value';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['table'] = 'field_data_field_coup_de_coeur';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['field'] = 'field_coup_de_coeur_value';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['value']['value'] = 'true';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['expose']['operator_id'] = 'field_coup_de_coeur_value_op';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['expose']['label'] = 'coup de coeur (field_coup_de_coeur)';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['expose']['operator'] = 'field_coup_de_coeur_value_op';
  $handler->display->display_options['filters']['field_coup_de_coeur_value']['expose']['identifier'] = 'field_coup_de_coeur_value';
  /* Critère de filtrage: Contenu : coup de coeur (field_coup_de_coeur) */
  $handler->display->display_options['filters']['field_coup_de_coeur_value_1']['id'] = 'field_coup_de_coeur_value_1';
  $handler->display->display_options['filters']['field_coup_de_coeur_value_1']['table'] = 'field_data_field_coup_de_coeur';
  $handler->display->display_options['filters']['field_coup_de_coeur_value_1']['field'] = 'field_coup_de_coeur_value';
  $handler->display->display_options['filters']['field_coup_de_coeur_value_1']['operator'] = '!=';
  $handler->display->display_options['filters']['field_coup_de_coeur_value_1']['value']['value'] = '0';

  /* Display: utilisateur */
  $handler = $view->new_display('panel_pane', 'utilisateur', 'panel_pane_1');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Filtre contextuel: Contenu : uid de l'auteur */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'node';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['exception']['title'] = 'Tout';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['pane_category']['name'] = 'Volets de vue';
  $handler->display->display_options['link_to_view'] = '0';
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: categorie */
  $handler = $view->new_display('panel_pane', 'categorie', 'panel_pane_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Filtre contextuel: Contenu : Possède un identifiant (ID) de terme de taxonomie */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['exception']['title'] = 'Tout';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['pane_category']['name'] = 'Volets de vue';
  $handler->display->display_options['inherit_panels_path'] = '1';
  $translatables['astuces'] = array(
    t('Master'),
    t('Mes astuces'),
    t('plus'),
    t('Appliquer'),
    t('Réinitialiser'),
    t('Trier par'),
    t('Asc'),
    t('Desc'),
    t('Éléments par page'),
    t('- Tout -'),
    t('Décalage'),
    t('« premier'),
    t('‹ précédent'),
    t('suivant ›'),
    t('dernier »'),
    t('Catégories'),
    t('note'),
    t('<img src="../sites/all/themes/robinDebrouille/img/chevron-en-savoir-plus.png"/>'),
    t('Date de publication'),
    t('Difficulté '),
    t('Contenu'),
    t('Body (body)'),
    t('Page'),
    t('Les astuces'),
    t('Bloc meilleur'),
    t('Les meilleurs'),
    t('Bloc dernier'),
    t('Les derniers'),
    t('Bloc coupe de coeur'),
    t('Les coups de coeur'),
    t('Catégorie'),
    t('Difficulté'),
    t('coup de coeur (field_coup_de_coeur)'),
    t('utilisateur'),
    t('Tout'),
    t('Volets de vue'),
    t('categorie'),
  );
  $export['astuces'] = $view;


  $view = new view();
  $view->name = 'menu_categorie';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'menu categorie';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'menu categorie';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'plus';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Appliquer';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Champ: Terme de taxonomie : Nom */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Critère de filtrage: Vocabulaire de taxonomie : Nom système */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'categorie' => 'categorie',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $translatables['menu_categorie'] = array(
    t('Master'),
    t('menu categorie'),
    t('plus'),
    t('Appliquer'),
    t('Réinitialiser'),
    t('Trier par'),
    t('Asc'),
    t('Desc'),
    t('Block'),
  );
  $export['menu_categorie'] = $view;

  return $export;
}
