<?php
/**
 * @file
 * robin_debrouille.layouts.inc
 */

/**
 * Implements hook_default_panels_layout().
 */
function robin_debrouille_astuce_default_panels_layout() {
  $export = array();

  $layout = new stdClass();
  $layout->disabled = FALSE; /* Edit this to true to make a default layout disabled initially */
  $layout->api_version = 1;
  $layout->name = 'page_d_accueil';
  $layout->admin_title = 'page d\'accueil';
  $layout->admin_description = '';
  $layout->category = '';
  $layout->plugin = 'flexible';
  $layout->settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
          2 => 2,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Pictos categories',
        'width' => 100,
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'illustration',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'illustration' => array(
        'type' => 'region',
        'title' => 'Illustration',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
        'class' => '',
      ),
      2 => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 5,
          1 => 4,
          2 => 3,
        ),
        'parent' => 'main',
        'class' => '',
      ),
      3 => array(
        'type' => 'column',
        'width' => '33.322026232473995',
        'width_type' => '%',
        'parent' => '2',
        'children' => array(
          0 => 8,
        ),
        'class' => '',
      ),
      4 => array(
        'type' => 'column',
        'width' => '33.29187396351575',
        'width_type' => '%',
        'parent' => '2',
        'children' => array(
          0 => 6,
        ),
        'class' => '',
      ),
      5 => array(
        'type' => 'column',
        'width' => '33.38609980401025',
        'width_type' => '%',
        'parent' => '2',
        'children' => array(
          0 => 7,
        ),
        'class' => '',
      ),
      6 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'les_coups_de_coueur',
        ),
        'parent' => '4',
        'class' => '',
      ),
      7 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'les_meilleurs',
        ),
        'parent' => '5',
        'class' => '',
      ),
      8 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'les_nouveaux',
        ),
        'parent' => '3',
        'class' => '',
      ),
      'les_meilleurs' => array(
        'type' => 'region',
        'title' => 'les meilleurs',
        'width' => 100,
        'width_type' => '%',
        'parent' => '7',
        'class' => '',
      ),
      'les_coups_de_coueur' => array(
        'type' => 'region',
        'title' => 'les coups de coueur',
        'width' => 100,
        'width_type' => '%',
        'parent' => '6',
        'class' => '',
      ),
      'les_nouveaux' => array(
        'type' => 'region',
        'title' => 'les nouveaux',
        'width' => 100,
        'width_type' => '%',
        'parent' => '8',
        'class' => '',
      ),
    ),
  );
  $export['page_d_accueil'] = $layout;

  return $export;
}
