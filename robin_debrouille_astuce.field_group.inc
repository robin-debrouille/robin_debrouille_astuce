<?php
/**
 * @file
 * robin_debrouille.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function robin_debrouille_astuce_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_astuce|node|astuce|form';
  $field_group->group_name = 'group_astuce';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'astuce';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'publis ton astuce',
    'weight' => '0',
    'children' => array(
      0 => 'field_partager',
      1 => 'path',
      2 => 'group_informations_principales',
      3 => 'group_informations_compl',
      4 => 'group_etapegroupe',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-astuce field-group-htabs',
      ),
    ),
  );
  $export['group_astuce|node|astuce|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_etapegroupe|node|astuce|form';
  $field_group->group_name = 'group_etapegroupe';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'astuce';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_astuce';
  $field_group->data = array(
    'label' => 'étape',
    'weight' => '3',
    'children' => array(
      0 => 'field_etapes',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-etapegroupe field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_etapegroupe|node|astuce|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_illustration_astuce|node|astuce|form';
  $field_group->group_name = 'group_illustration_astuce';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'astuce';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_informations_compl';
  $field_group->data = array(
    'label' => 'illustration',
    'weight' => '34',
    'children' => array(
      0 => 'field_iilustration_astuce',
      1 => 'field_credit',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-illustration-astuce field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_illustration_astuce|node|astuce|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_illustration_etape|node|etape|form';
  $field_group->group_name = 'group_illustration_etape';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'etape';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'illustration',
    'weight' => '1',
    'children' => array(
      0 => 'field_iilustration',
      1 => 'field_credit',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-illustration-etape field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_illustration_etape|node|etape|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_informations_compl|node|astuce|form';
  $field_group->group_name = 'group_informations_compl';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'astuce';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_astuce';
  $field_group->data = array(
    'label' => 'Informations complémentaires',
    'weight' => '2',
    'children' => array(
      0 => 'field_besoins',
      1 => 'field_attention',
      2 => 'field_note',
      3 => 'field_coup_de_coeur',
      4 => 'field_tag_sous_categorie',
      5 => 'field_temp',
      6 => 'group_illustration_astuce',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_informations_compl|node|astuce|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_informations_comp|node|astuce|default';
  $field_group->group_name = 'group_informations_comp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'astuce';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Informations complémentaires',
    'weight' => '1',
    'children' => array(
      0 => 'field_cat_gorie',
      1 => 'field_besoins',
      2 => 'field_attention',
      3 => 'field_note',
      4 => 'field_difficulte',
      5 => 'field_tag_sous_categorie',
      6 => 'field_temp',
      7 => 'og_group_ref',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Informations complémentaires',
      'instance_settings' => array(
        'classes' => 'field-group-format group_informations_comp field-group-div group-informations-comp  speed-none effect-none',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_informations_comp|node|astuce|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_informations_principales|node|astuce|default';
  $field_group->group_name = 'group_informations_principales';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'astuce';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Informations principales',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_iilustration_astuce',
      2 => 'field_credit',
      3 => 'field_etapes',
      4 => 'field_partager',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Informations principales',
      'instance_settings' => array(
        'classes' => 'field-group-format group_informations_principales field-group-div group-informations-principales  speed-none effect-none',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_informations_principales|node|astuce|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_informations_principales|node|astuce|form';
  $field_group->group_name = 'group_informations_principales';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'astuce';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_astuce';
  $field_group->data = array(
    'label' => 'informations principales',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_cat_gorie',
      2 => 'field_difficulte',
      3 => 'title',
      4 => 'field_licence',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_informations_principales|node|astuce|form'] = $field_group;

  return $export;
}
