<?php

/**
 * Implements hook_drush_command().
 */
function robin_debrouille_astuce_drush_command() {

  $items['import-taxonomy-astuce'] = array(
    'description' => 'importe les taxonomies d\'astuce.',
    'aliases' => array('rd-ica'),
  );

  return $items;
}

function drush_robin_debrouille_astuce_import_taxonomy_astuce() {
    $vocab = taxonomy_vocabulary_machine_name_load('categorie');

    $term1 = (object) array(
     'name' => 'Bricolage',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file1 = (array) file_save_data(
        file_get_contents(
            DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
            .'/image_categories/bricolage.png'
        ),'public://categorie-astuce/bricolage.png');
    $term1->field_icone[LANGUAGE_NONE][0] = $file1;
    taxonomy_term_save($term1);

    $term2 = (object) array(
     'name' => 'Linge',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file2 = (array) file_save_data(
        file_get_contents(
            DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
            .'/image_categories/linge.png'
        ),'public://categorie-astuce/linge.png');
    $term2->field_icone[LANGUAGE_NONE][0] = $file2;
    taxonomy_term_save($term2);

    $term3 = (object) array(
     'name' => 'Maison',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file3 = (array) file_save_data(
        file_get_contents(
            DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
            .'/image_categories/maison.png'
        ),'public://categorie-astuce/maison.png');
    $term3->field_icone[LANGUAGE_NONE][0] = $file3;
    taxonomy_term_save($term3);

    $term4 = (object) array(
     'name' => 'Cuisine',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file4 = (array) file_save_data(
        file_get_contents(
            DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
            .'/image_categories/cuisine.png'
        ),'public://categorie-astuce/cuisine.png');
    $term4->field_icone[LANGUAGE_NONE][0] = $file4;
    taxonomy_term_save($term4);

    $term5 = (object) array(
     'name' => 'Couture',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file5 = (array) file_save_data(
        file_get_contents(
            DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
            .'/image_categories/couture.png'
        ),'public://categorie-astuce/couture.png');
    $term5->field_icone[LANGUAGE_NONE][0] = $file5;
    taxonomy_term_save($term5);

    $term6 = (object) array(
     'name' => 'Jardin',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file6 = (array) file_save_data(
        file_get_contents(
            DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
            .'/image_categories/jardin.png'
        ),'public://categorie-astuce/jardin.png');
    $term6->field_icone[LANGUAGE_NONE][0] = $file6;
    taxonomy_term_save($term6);

    $term7 = (object) array(
     'name' => 'Animaux',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file7 = (array) file_save_data(
        file_get_contents(
            DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
            .'/image_categories/animaux.png'
        ),'public://categorie-astuce/animaux.png');
    $term7->field_icone[LANGUAGE_NONE][0] = $file7;
    taxonomy_term_save($term7);

    $term8 = (object) array(
     'name' => 'Informatique',
     'description' => '',
     'vid' => $vocab->vid,
    );
    $file8 = (array) file_save_data(
         file_get_contents(
             DRUPAL_ROOT.'/'.drupal_get_path('module', 'robin_debrouille_astuce')
             .'/image_categories/electrique.png'
         ),'public://categorie-astuce/electrique.png');
    $term8->field_icone[LANGUAGE_NONE][0] = $file8;
    taxonomy_term_save($term8);

    $vocab = taxonomy_vocabulary_machine_name_load('difficulte');

    $term1 = (object) array(
     'name' => 'très difficille',
     'description' => '',
     'vid' => $vocab->vid,
    );
    taxonomy_term_save($term1);

    $term2 = (object) array(
     'name' => 'difficile',
     'description' => '',
     'vid' => $vocab->vid,
    );
    taxonomy_term_save($term2);

    $term3 = (object) array(
     'name' => 'moyen',
     'description' => '',
     'vid' => $vocab->vid,
    );
    taxonomy_term_save($term3);

    $term4 = (object) array(
     'name' => 'facile',
     'description' => '',
     'vid' => $vocab->vid,
    );
    taxonomy_term_save($term4);

    $term5 = (object) array(
     'name' => 'très facile',
     'description' => '',
     'vid' => $vocab->vid,
    );
    taxonomy_term_save($term5);

}