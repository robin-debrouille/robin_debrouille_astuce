<?php

/**
 * @file
 * robin_debrouille_astuce.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function robin_debrouille_astuce_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'Le <em>menu Principal</em> est fréquemment utilisé pour afficher les sections importantes du site, souvent dans la barre de navigation de haut de page.',
  );
  // Exported menu: menu-footer.
  $menus['menu-footer'] = array(
    'menu_name' => 'menu-footer',
    'title' => 'Footer',
    'description' => 'Menu du footer permettant la redirection vers les pages statiques - pieds de page',
  );
  // Exported menu: menu-menu-segondaire.
  $menus['menu-menu-segondaire'] = array(
    'menu_name' => 'menu-menu-segondaire',
    'title' => 'Menu segondaire',
    'description' => '',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'Le menu <em>Utilisateur</em> contient les liens du compte utilisateur, comme le lien "Déconnexion"',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Footer');
  t('Le <em>menu Principal</em> est fréquemment utilisé pour afficher les sections importantes du site, souvent dans la barre de navigation de haut de page.');
  t('Le menu <em>Utilisateur</em> contient les liens du compte utilisateur, comme le lien "Déconnexion"');
  t('Main menu');
  t('Menu du footer permettant la redirection vers les pages statiques - pieds de page');
  t('Menu segondaire');
  t('User menu');

  return $menus;
}
