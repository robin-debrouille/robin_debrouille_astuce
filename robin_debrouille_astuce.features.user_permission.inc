<?php

/**
 * @file
 * robin_debrouille_astuce.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function robin_debrouille_astuce_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_coup_de_coeur'.
  $permissions['create field_coup_de_coeur'] = array(
    'name' => 'create field_coup_de_coeur',
    'roles' => array(
      'administrator' => 'administrator',
      'moderateur' => 'moderateur',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_coup_de_coeur'.
  $permissions['edit field_coup_de_coeur'] = array(
    'name' => 'edit field_coup_de_coeur',
    'roles' => array(
      'administrator' => 'administrator',
      'moderateur' => 'moderateur',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_coup_de_coeur'.
  $permissions['edit own field_coup_de_coeur'] = array(
    'name' => 'edit own field_coup_de_coeur',
    'roles' => array(
      'administrator' => 'administrator',
      'moderateur' => 'moderateur',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_coup_de_coeur'.
  $permissions['view field_coup_de_coeur'] = array(
    'name' => 'view field_coup_de_coeur',
    'roles' => array(
      'administrator' => 'administrator',
      'moderateur' => 'moderateur',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_coup_de_coeur'.
  $permissions['view own field_coup_de_coeur'] = array(
    'name' => 'view own field_coup_de_coeur',
    'roles' => array(
      'administrator' => 'administrator',
      'moderateur' => 'moderateur',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
